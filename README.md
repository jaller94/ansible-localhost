# Ansible for localhost

Prepare
```
wget https://gitlab.com/jaller94/ansible-localhost/-/archive/master/ansible-localhost-master.zip
unzip ansible-localhost-master.zip
cd ansible-localhost-master
sudo pacman -Syyu ansible-base
# Install for pacman
sudo ansible-galaxy collection install community.general
```

Run
```
sudo ansible-playbook site.yml
```
